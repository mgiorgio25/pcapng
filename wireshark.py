header = []
version = []
body = []
byteOrder = "little"


def header_func(list):
    global byteOrder
    print(list[0].hex()) # block type
    header_lenght = int.from_bytes(list[1], byteOrder, signed=False)
    print(int.from_bytes(list[1], byteOrder, signed=False))
    # Magic Number Section
    magicNumber = list[2].hex()
    if magicNumber == "4d3c2b1a":
        byteOrder = "little"
    elif magicNumber == "a1b2c3d4":
        byteOrder = "big"
    if byteOrder == "little":
        magicNumber_len = len(magicNumber)
        magicNumber = magicNumber[magicNumber_len::-1]
    print("2: " + str(magicNumber))
    # print(list[2].hex())

    # Version of file
    print("3: " + str(list[3].hex()))
    # print("4: "+str(list[4].hex())) # empty
    # print("5: "+str(list[5].hex())) # empty
    # SnapLen
    print("6: " + str(list[6].hex()))
    snapLen = int.from_bytes(list[6], byteOrder, signed=False)
    print(snapLen)
    # LinkType
    print("7: " + str(list[7]))


with open("60_qos_0.pcapng", "rb") as f:
    for n in range(0, 8):
        byte = f.read(4)
        header.append(byte)
    header_func(header)

    for n in range(0, 7):
        if n < 6:
            byte = f.read(29)
        if n == 6:
            byte = f.read(10)
        version.append(byte)
    # print(version)

    # for n in range(0, 9):
    #     byte = f.read(4)
    #     body.append(byte)
    # print("0: " + str(int.from_bytes(body[0], byteOrder, signed=False)))
    # print("1: " + str(int.from_bytes(body[1], byteOrder, signed=False)))
    # print("2: " + str(int.from_bytes(body[2], byteOrder, signed=False)))
    # print("3: " + str(int.from_bytes(body[3], byteOrder, signed=False)))
    # print("4: " + str(int.from_bytes(body[4], byteOrder, signed=False)))
    # print("5: " + str(int.from_bytes(body[5], byteOrder, signed=False)))
    # print("6: " + str(int.from_bytes(body[6], byteOrder, signed=False)))
    # print("7: " + str(int.from_bytes(body[7], byteOrder, signed=False)))
    # print("8: " + str(int.from_bytes(body[8], byteOrder, signed=False)))
    # print(body)