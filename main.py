from header import *
from interface import *
from data import *

with open("60_qos_0.pcapng", "rb") as f:
    process_header(f)
    process_interface(f, byteOrder)
    n = 1
    byte = f.read(4)
    while byte != b"":
        print("\nID: "+str(n))
        data_type = int.from_bytes(byte, byteOrder, signed=False)
        print("data_type: "+str(data_type))
        if data_type == 6:
            process_epb(f, byteOrder)
        else:
            exit()

        byte = f.read(4)
        n += 1

    # data_type = int.from_bytes(f.read(4), byteOrder, signed=False)
    # print("\ndata_type: " + str(data_type))
    # if data_type == 6:
    #     process_epb(f, byteOrder)
    # data_type = int.from_bytes(f.read(4), byteOrder, signed=False)
    # print("\ndata_type: " + str(data_type))
