byteOrder = "little"

def process_header(file):
    file.read(4)
    header_lenght = int.from_bytes(file.read(4), byteOrder, signed=False)
    # print("header_lenght: "+str(header_lenght))
    variable_lenght = header_lenght - 28
    # print("variable_lenght: " + str(variable_lenght))
    magic_number = file.read(4)
    process_magicNumber(magic_number)
    major_version = int.from_bytes(file.read(2), byteOrder, signed=False)
    print("major_version: "+str(major_version))
    minor_version = int.from_bytes(file.read(2), byteOrder, signed=False)
    print("minor_version: " + str(minor_version))
    section_lenght = int.from_bytes(file.read(8), byteOrder, signed=True)
    print("section_lenght: " + str(section_lenght))
    options = file.read(variable_lenght)
    # print("options: " + str(options))
    header_lenght_end = int.from_bytes(file.read(4), byteOrder, signed=False)
    # print("header_lenght_end: "+str(header_lenght_end))


def process_magicNumber(data):
    global byteOrder
    # Magic Number Section
    magicNumber = data.hex()
    if magicNumber == "4d3c2b1a":
        byteOrder = "little"
    elif magicNumber == "a1b2c3d4":
        byteOrder = "big"
    if byteOrder == "little":
        magicNumber_len = len(magicNumber)
        magicNumber = magicNumber[magicNumber_len::-1]

    return magicNumber